//Function PArameters and Arguments

//"name" is called a parameter that acts as a named variable/ container that exist only inside of the function
//"Janel" - argument - it is the information/data provided directly into the function. 

function printInput(name) {
	console.log("My Name is " + name);

}
printInput("Janel");

let sampleVariable = "Yuri";
printInput(sampleVariable);


function checkDivisibilityBy8(num){
                let remainder = num % 8;
                console.log("The remainder of " + num + " divided by 8 is: " + remainder);
                let isDivisibleBy8 = remainder === 0;
                console.log("Is " + num + " divisible by 8?");
                console.log(isDivisibleBy8);
            }

            checkDivisibilityBy8(64);
            checkDivisibilityBy8(28);

//Functions as Arguments

//Functions parameters can also accept other functions as arguments

function argumentFunction(){
	console.log("This function was passed as an argumebt before the message was printed");
}
function invokeFunction(argumentFunction){
	argumentFunction();

	console.log("This code comes from invokeFunction");
}

invokeFunction(argumentFunction);

//Multiple Arguments - will correspond to the number of "Parameters" declared in a function in succeeding order.


function createFullName(firstName, middleName, lastName){
	console.log(firstName + ' ' + middleName +' ' + lastName);
}

createFullName('Juane', 'Dela');
createFullName('Jane', 'Dela', 'Cruz');
createFullName('Jane', 'Dela', 'Cruz', 'hello');


let firstName ="john";
let middleName = "Doe";
let lastName = "smith";

createFullName(firstName, middleName,lastName);


//The return Statements - allows us to output a value from a function to be passed to the line/block of code that invoked.called the functions. 

//The return statement also stops the execution of the function and any code after the return statement


function returnFullName(firstName, middleName, lastName){

	return firstName + ' ' + middleName + ' ' + lastName;
	console.log("this messages will not be printed");
}
let completeName = returnFullName('Joe','Jayson', 'Helbert');
console.log(completeName);

function returnAddress(city, country){
    let fullAddress = city + ", " + country;
    return fullAddress;

}

let myAddress = returnAddress("Cebu City", "Philippines");

console.log(myAddress)
